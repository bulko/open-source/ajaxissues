<?php
include "function.php";
if( 
	empty($_REQUEST)
	|| empty($_REQUEST["token"])
	|| empty($_REQUEST["name"])
	|| $_REQUEST["token"] != sha1( date("Y-m-d") . md5( $_REQUEST["name"] ) )
)
{
	if( $_SERVER['REMOTE_ADDR'] != '82.245.97.150' && $_SERVER['REMOTE_ADDR'] != '127.0.0.1' )
	{
		display403();
	}
	else{
		$debugInfo = array(
			"browserName" => getBrowserName(),
			"platform" => getPlatform(),
			"do_not_track" => getDntStatus(),
			"ip" => $_SERVER['REMOTE_ADDR'],
			"userAgent" => $_SERVER['HTTP_USER_AGENT'],
			"lang" => substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2),
			"OS" => PHP_OS,
			"OSUser" => php_uname(),
			"root" => true
		);
	}
}
else{
	$debugInfo = array(
		"browserName" => getBrowserName(),
		"platform" => getPlatform(),
		"do_not_track" => getDntStatus(),
		"ip" => $_SERVER['REMOTE_ADDR'],
		"userAgent" => $_SERVER['HTTP_USER_AGENT'],
		"lang" => substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2),
		"OS" => PHP_OS,
		"OSUser" => php_uname(),
		"root" => false
	);
}
?>
