<?php
	include "process.php"
?>
<!DOCTYPE html>
	<html lang="fr">
	<head>
		<title>Bulko issues</title>
		<meta charset="utf-8">
		<meta name="viewport" content="user-scalable=no, initial-scale = 1, minimum-scale = 1, maximum-scale = 1, width=device-width">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
		<link rel="image_src" href="http://www.bulko.net/templates/img/logo_bulko.jpg" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="http://www.bulko.net/templates/img/favicon.ico" />
		<link rel="shortcut icon" type="image/x-icon" href="http://www.bulko.net/templates/img/favicon.ico" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans|Oswald|Material+Icons" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
		<link href="css/global.css" rel="stylesheet">
	</head>
	<body>
		<main id="page">
			<header id="page-header" class="page-header black">
				<nav class="container black">
					<div class="nav-wrapper">
						<a href="" class="main-logo">
							<img src="./img/logo.png" alt="BKO" />
						</a>
						<?php
						if ( isset($_REQUEST["back"]) )
						{
							if ( isset( $_REQUEST["img"] ) )
							{
								echo '<a href="http://'. $_REQUEST["back"].'" class="brand-logo"><img src="http://'.$_REQUEST["img"].'" alt="'.$_REQUEST["name"].'" /><span>&nbsp;&nbsp;rapport de bug</span></a>';
							}
							elseif ( file_exists( "img/logo-repo/".$_REQUEST["name"].".png" ) )
							{
								echo '<a href="http://'. $_REQUEST["back"].'" class="brand-logo"><img src="img/logo-repo/'.$_REQUEST["name"].'.png" /><span>&nbsp;&nbsp;rapport de bug</span></a>';
							}
							else
							{
								echo '<a href="http://'. $_REQUEST["back"].'" class="brand-logo">'.$_REQUEST["name"].'&nbsp;&nbsp;rapport de bug</a>';
							}
						}
						?>
						<ul class="right hide-on-med-and-down">
							<li class="active reload">Ouvrir un ticket</li>
							<li class="getIssues">Voir la liste des tickets</li>
						</ul>
					</div>
				</nav>
			</header>
  			<!-- Modal Structure modalAnswer -->
			<div id="modalAnswer" class="modal bottom-sheet">
				<div class="modal-content">
					<h4>Modal Header</h4>
					<p>A bunch of text</p>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect waves-light btn black">Fermer</a>
				</div>
			</div>
			<!-- Modal Structure modalAnswer End-->
			<form id="submitMyIssue">
				<!-- <div class="center"> -->
					<div class="form-wrapper">
						<div id="submitMessage" class="card green hide">
							<div class="card-content white-text">
								<span class="card-title"> <i class="small material-icons">info_outline</i> Votre rapport nous a bien été transmis</span>
								<p>Nous revenons vers vous dans les plus brefs délais. </p>
								<?php
								if ( isset($_REQUEST["back"]) ):
								?>
								<a class="waves-effect waves-light btn right green accent-4green accent-4" href="<?php echo 'http://'. $_REQUEST["back"] ?>">Revenir sur mon site</a>
								<div class="clear"></div>
								<?php
								endif;
								?>
							</div>
						</div>
						<div  id="GHError" class="card red hide">
								<div class="card-content white-text">
									<span class="card-title"> <i class="small material-icons">error_outline</i> Une erreur inconnue est survenu.</span>
									<p>Merci de nous transmettre par email (r-ro@bulko.net) les informations suivantes:</p>
									<textarea></textarea>
									<p>Nous vous remercions pour votre compréhension.</p>
								</div>
							</div>
						<?php 
						if ($debugInfo["root"])
						{
						?>
							<div class="card orange">
								<div class="card-content white-text">
									<span class="card-title"> <i class="small material-icons">info_outline</i> Vous êtes connecté en mode ROOT via <?php echo $_SERVER['REMOTE_ADDR'] ?></span>
								</div>
							</div>

							<div class="form-start input-field col s6">
								<i class="material-icons prefix">vpn_key</i>
								<input  type="text" name="nameRepo" class="required validate" required="" value="<?php if(isset($_REQUEST["name"])){ echo $_REQUEST["name"]; } ?>">
								<label for="icon_prefix">Nom du repo *</label>
							</div>
							<div class="input-field col s6">
								<i class="material-icons prefix">mode_edit</i>
								<input  type="text" name="title" class="required validate" required="">
								<label for="icon_prefix">Titre *</label>
							</div>
						<?php
						}
						else
						{
						?>
						<input type="hidden" name="nameRepo" placeholder="Nom du repo" value='<?php echo $_REQUEST["name"]; ?>' class="grey-text text-darken-4"/>
						<div class="form-start input-field col s6">
							<i class="material-icons prefix">mode_edit</i>
							<input  type="text" name="title" class="required validate" required="">
							<label for="icon_prefix">Titre *</label>
						</div>
						<?php
						}
						?>
						<div class="input-field col s6">
							<i class="material-icons prefix">account_circle</i>
							<input  type="text" class="validate" id="userpersonalinfo" value=""/>
							<label for="icon_prefix">On vous rappelle? (téléphone ou email)</label>
						</div>
						<div class="input-field col s12">
							<i class="material-icons prefix">error_outline</i>
							<select name="labels">
								<option value="" disabled selected>Vous voulez:</option>
								<option value="question">Poser une question</option>
								<option value="help wanted">Demander de l'aide</option>
								<option value="enhancement">Demander une amélioration</option>
								<option value="bug">Signaler un bug</option>
							</select>
							<!-- <label>Materialize Multiple Select</label> -->
						 </div>
						<input type="file" name="files[]" id="filer_input2" multiple="multiple" data-jfiler-limit="3" data-jfiler-extensions="jpg,png,gif">
						<textarea class="simplemde" name="body" placeholder="Descriptif du bug *"></textarea>
						<button class="waves-effect waves-light btn black right">
							<i class="material-icons right">send</i> Nous signaler un bug
						</button>
					</div>
				<!-- </div> -->
				<input type="hidden" name="userinfo" id="userinfo" value="<?php foreach( $debugInfo as $key => $info) {
											echo "* " . $key .' : ' .$info . "  \\n";	
											} ?>" >
			</form>
			<div id="readMyIssue" style="display:none;">
				<table class="highlight  responsive-table">
					<thead>
						<tr>
							<th data-field="title" >Titre</th>
							<th data-field="label" >Label</th>
							<th data-field="responce" >Réponses</th>
							<th data-field="last_update" >Dernière mise à jour</th>
							<th data-field="create" >Création</th>
						</tr>
					</thead>
					<tbody id="issues">

					</tbody>
				</table>
			</div>
			<footer id="page-footer" class="page-footer black">
				<div class="container">
					<div class="row">
						<div class="col s12">
							<ul>
								<li class='left'>
									<a href="http://bulko.net">
										© 2016 Bulko.net
									</a>
								</li>
								<li class='right'>
									<a href="http://agencebulko.tumblr.com/">
										<i class="fa fa-tumblr" aria-hidden="true"></i>
									</a>
								</li>
								<li class='right'>
									<a href="https://twitter.com/AgenceBulko/">
										<i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
								</li>
								<li class='right'>
									<a href="https://plus.google.com/112088430758924491251">
										<i class="fa fa-google-plus-official" aria-hidden="true"></i>
									</a>
								</li>
								<li class='right'>
									<a href="http://github.com/bulko/">
										<i class="fa fa-github" aria-hidden="true"></i>
									</a>
								</li>
								<li class='right'>
									<a href="https://www.linkedin.com/company/9998827?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A9998827%2Cidx%3A3-1-5%2CtarId%3A1472741829911%2Ctas%3Abulko">
										<i class="fa fa-linkedin" aria-hidden="true"></i>
									</a>
								</li>
								<li class='right'>
									<a href="https://www.facebook.com/BULKO-69705565491/">
										<i class="fa fa-facebook-official" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</footer>
		</main>
		<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.1.0.min.js" ntegrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
		<script src="https://cdn.rawgit.com/showdownjs/showdown/1.4.3/dist/showdown.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
		<script src="js/jquery.filer.min.js" type="text/javascript"></script>
		<script src="js/customUpload.js" type="text/javascript"></script>
		<script src="js/app.js"></script>
	</body>
</html>
