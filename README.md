# ajaxIssues

## Exemple classique PHP

`define("ISSUES_URL", "http://issues.bko911.com/index.php?token=" . sha1( date("Y-m-d") . md5( "nom-repo-git-hub" ) )."&name=nom-repo-git-hub[&back=url-retour-sans-http]&img=[&back=url-image-sans-http]" );`
`<a href="ISSUES_URL" title="Signaler un bug">Signaler un bug</a>`

## Exemple Wordpress (admin)

		public function createRootTopHook( WP_Admin_Bar $wp_admin_bar )
		{
			global $current_user;
	
			$wp_admin_bar->add_node( array(
						'id' => 'vdo_bkobug_shortcut',
						'title' => __('Signaler un bug'),
						'href' => "http://issues.bko911.com/index.php?token=" . sha1( date("Y-m-d") . md5( "nom-repo-git-hub ) )."&name=nom-repo-git-hub[&back=url-retour-sans-http]&img=[&back=url-image-sans-http]g",
						'meta' => array( 'class' => 'wp-menu-image dashicons-before dashicons-admin-links' ),
						'parent' => false
					));
			return true;
		}
		add_action( 'admin_bar_menu', array( $this, 'createRootTopHook' ), 50 );


## Exemple Prestashop/Smarty (admin)

Dans le settings.ing.php

		definedIfOk('_BULKOISSUES_REPOSITORY_NAME_', 'nom-repo-git-hub');
		definedIfOk('_BULKOISSUES_URL_BACK_', "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
		definedIfOk('_BULKOISSUES_IMG_', "url-image-sans-http");
		definedIfOk('_BULKOISSUES_URL_', "http://issues.bko911.com/index.php?token=");

Dans le header.tpl 

		{assign var=nameRepo value=$smarty.const._BULKOISSUES_REPOSITORY_NAME_}
		{assign var="urlBack" value=$smarty.const._BULKOISSUES_URL_BACK_}
		{assign var="img" value=$smarty.const._BULKOISSUES_IMG_}
		{assign var=baseUrl value=$smarty.const._BULKOISSUES_URL_}
		{assign var=token value=$nameRepo|md5}
		{assign var=key value=$smarty.now|date_format:"%Y-%m-%d"|cat:$token|sha1}
		{assign var=param value="&name="|cat:$nameRepo|cat:"&back="|cat:$urlBack|cat:"&img="|cat:$img}
		{assign var=issuesUrl value=$baseUrl|cat:$key|cat:$param}

La ou vous voulez votre lien

		<a class="toolbar_btn btn-help" href="{$issuesUrl}" title="{l s='Nous signaler un bug'}" target="_blank">
			<i class="icon-bug"></i>
		</a>


## Exemple Laravel

		<a class="toolbar_btn btn-help" href="http://issues.bko911.com/index.php?token=" . sha1( date("Y-m-d") . md5( "nom-repo-git-hub" ) )."&name=nom-repo-git-hub[&back=url-retour-sans-http]&img=[&back=url-image-sans-http]" title="Nous signaler un bug" target="_blank">
			<i class="fa fa-btn fa-bug"></i>
		</a>
